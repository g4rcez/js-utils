import moment from "moment";

const sortByDateTime = (a, b) => {
	if (moment(a).isSame(moment(b))) {
		return 0;
	} else {
		if (moment(a).isAfter(moment(b))) {
			return 1;
		} else {
			return -1;
		}
	}
};

const alphabeticSort = (a, b) => {
	const lowerA = String(a).toLocaleLowerCase;
	const lowerB = String(b).toLocaleLowerCase;
	if (lowerA == lowerB) {
		return 0;
	} else {
		if (lowerA > lowerB) {
			return 1;
		} else {
			return -1;
		}
	}
};

export { alphabeticSort, sortByDateTime };
