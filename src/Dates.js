import moment from "moment";

const masks = {
	default: "ddd MMM dd YYYY HH:mm:ss",
	short: "M/D/YYYY",
	medium: "MMM D, YY",
	isoDate: "YYYY-MM-DD",
	isoDatetime: "YYYY-MM-DD'T'HH:mm:ss",
	isoUtcDatetime: "UTC:YYYY-MM-DD'T'HH:mm:ss'Z",
	unixTimeStamps: "X",
	unixTimeStampsMs: "x",
	htmlDate: this.isoDate,
	htmlDateTimeLocal: "YYYY-MM-DDTHH:mm",
	htmlTime: "HH:mm",
	hour: "HH:mm:ss",
};

const dateFormat = (date, format = "DD MMM") => moment(date).format(format);
const timeFormat = (date, format = "HH:MM") => moment(date).format(format);
const afterToday = (dateCompare) => moment(dateCompare).isAfter(moment(), "day");
const today = (dateCompare) => moment(dateCompare).isSame(moment(), "day");
const sameMouth = (date) => moment(date).isSame(moment(), "month");
const sameYear = (date) => moment(date).isSame(moment(), "year");
const addDays = (date, days) => moment(date).add(days);
const subtractDays = (date, days) => moment(date).subtract(days);

export default {
	parts,
	today,
	subtractDays,
	addDays,
	masks,
	afterToday,
	timeFormat,
	sameMouth,
	dateFormat,
	sameYear,
};
