const classesManager = (id, operation, classes = []) => {
	let el = document.getElementById(id);
	if (el !== undefined) {
		classes.forEach((x) => el.classList[operation](x));
		return true;
	}
	return false;
};

const elementVisible = (id, hide) => {
	let el = document.getElementById(id);
	if (el !== undefined) {
		el.hidden = hide;
	}
};

const selector = (selectors) => {
	return document.querySelector(selectors);
};

const listClass = (id) => {
	const el = document.getElementById(id);
	return el.classList.contains(className);
};

const getClasses = (el) => el.classList();

export default {
	addClasses: (id, ...classes) => {
		classesManager(id, "add", classes);
	},
	removeClasses: (id, ...classes) => {
		classesManager(id, "remove", classes);
	},
	copyToClipboard: (id) => {
		let text = document.getElementById(id);
		text.select();
		document.execCommand("copy");
		text.blur();
	},
	bodyStyle: (prop, value) => (document.body.style[prop] = value),
	changeTitle: (value) => (document.title = value),
	hideElement: (id) => elementVisible(id, true),
	showElement: (id) => elementVisible(id, false),
	changeElement: (id, newElement) => {
		let el = document.getElementById(id);
		if (el !== undefined) {
			el.innerHTML = newElement;
			el.id = id;
		}
	},
	hiddenTab: () => document.hidden,
	selector,
	listClass,
	getClasses,
};
