import StorageManage from "storage-manager-js";
import axios from "axios";

const storage = StorageManage("cookie");
const Authorization = storage.get("credenciais");
const headers = {
  Authorization
};

export default {
  get: endpoint => axios.get(endpoint, { headers }),
  post: (endpoint, data) => axios.post(endpoint, data, { headers }),
  put: (endpoint, data) => axios.post(endpoint, data, { headers }),
  delete: endpoint => axios.delete(endpoint)
};
