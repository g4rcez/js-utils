import Axios from "axios";
import StorageManage from "./StorageManage";

const storage = new StorageManage("cookie");

export default {
	send: async (url, query) => {
		return await Axios.post(
			url,
			{ query: query },
			{
				headers: {
					"Content-Type": "application/json",
					Authorization: storage.get("credenciais"),
				},
			},
		);
	},
	query: (name, ...values) => {
		let string = "";
		values.forEach((x) => (string = string.concat(`${x} `)));
		return `{ ${name.trim()} { ${string.trim()} } }`;
	},
	findBy: (name, search, searchValue, ...values) => {
		let string = `${name}(${search}:${searchValue}) { `;
		values.forEach((x) => (string = string.concat(`${x} `)));
		return `{ ${string.trim()} } }`;
	},
	object: (queryName, ...parameters) => {
		let string = "";
		parameters.forEach((x) => (string = string.concat(`${x} `)));
		return `{ ${queryName.trim()} { ${string.trim()} }}`;
	},
};
