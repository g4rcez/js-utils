import Keys from "./Keys";
import Urls from "./Urls";
import Dates from "./Dates";
import Fetch from "./Fetch";
import Arrays from "./Arrays";
import Devices from "./Devices";
import Strings from "./Strings";
import DomUtils from "./DomUtils";
import Functional from "./Functional";
import Validations from "./Validations";
import SortFunctions from "./SortFunctions";
import GraphQLClient from "./GraphQLClient";

export default {
	Keys,
	Urls,
	Dates,
	Fetch,
	Arrays,
	Devices,
	Strings,
	Devices,
	DomUtils,
	Functional,
	Validations,
	SortFunctions,
	GraphQLClient,
};
