const isCPF = (cpf) => {
	let numbers, digits, sum, i, result, sames;
	sames = 1;
	if (cpf.length < 11 || cpf === "00000000000") return false;
	for (i = 0; i < cpf.length - 1; i++)
		if (cpf.charAt(i) != cpf.charAt(i + 1)) {
			sames = 0;
			break;
		}
	if (!sames) {
		numbers = cpf.substring(0, 9);
		digits = cpf.substring(9);
		sum = 0;
		for (i = 10; i > 1; i--) sum += numbers.charAt(10 - i) * i;
		result = sum % 11 < 2 ? 0 : 11 - (sum % 11);
		if (result != digits.charAt(0)) return false;
		numbers = cpf.substring(0, 10);
		sum = 0;
		for (i = 11; i > 1; i--) sum += numbers.charAt(11 - i) * i;
		result = sum % 11 < 2 ? 0 : 11 - (sum % 11);
		if (result != digits.charAt(1)) return false;
		return true;
	} else return false;
};

const isEmail = (email) => {
	let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
};

const isCNPJ = (cnpj) => {
	cnpj = cnpj.replace(/[^\d]+/g, "");
	if (cnpj == "00000000000000" || cnpj.length != 14) {
		return false;
	}
	let length = cnpj.length - 2;
	let numbers = cnpj.substring(0, length);
	let digits = cnpj.substring(length);
	let sum = 0;
	let pos = length - 7;
	for (let i = length; i >= 1; i--) {
		sum += numbers.charAt(length - i) * pos--;
		if (pos < 2) pos = 9;
	}
	let resultado = sum % 11 < 2 ? 0 : 11 - (sum % 11);
	if (resultado != digits.charAt(0)) return false;
	length = length + 1;
	numbers = cnpj.substring(0, length);
	sum = 0;
	pos = length - 7;
	for (let i = length; i >= 1; i--) {
		sum += numbers.charAt(length - i) * pos--;
		if (pos < 2) pos = 9;
	}
	resultado = sum % 11 < 2 ? 0 : 11 - (sum % 11);
	if (resultado != digits.charAt(1)) {
		return false;
	}
	return true;
};

const isType = (type, val) => ![, null].includes(val) && val.constructor === type;

const isJson = (json) => {
	try {
		JSON.parse(json);
		return true;
	} catch (e) {
		return false;
	}
};

const equals = (a, b) => {
	if (a === b) return true;
	if (a === null || a === undefined || b === null || b === undefined) return false;
	if (a.prototype !== b.prototype) return false;
	if (keys.length !== Object.keys(b).length) return false;
	if (!a || !b || (typeof a !== "object" && typeof b !== "object")) return a === b;
	if (a instanceof Date && b instanceof Date) return a.getTime() === b.getTime();
	let keys = Object.keys(a);
	return keys.every((k) => equals(a[k], b[k]));
};

export default {
	isCPF,
	today,
	equals,
	isCNPJ,
	isType,
	isJson,
	isEmail,
	afterToday,
};
