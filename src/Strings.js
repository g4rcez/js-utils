const capitalize = (string) => {
	string = string.toLowerCase();
	let newString = string.replace(/\b[a-z]/g, (char) => char.toUpperCase());
	return newString
		.trim()
		.replace(" De ", " de ")
		.replace(" Da ", " da ")
		.replace(" Do ", " do ")
		.replace(" Dos ", " dos ")
		.replace(" Das ", " das ")
		.replace(" Del ", " del ");
};

const safe = (string) => new String(string);

const firstName = (fullName) => {
	let parts = fullName.split(" ");
	if (parts.length > 1) return parts[0];
	return fullName;
};

const clearHtml = (str) => str.replace(/<[^>]*>/g, "");

const currencyFormat = (value, intl = "pt-BR", currency = "BRL") =>
	new Intl.NumberFormat(intl, {
		style: "currency",
		currency,
	}).format(value);

const floatPrecision = (valor, precision = 2) => parseFloat(valor).toFixed(precision);

const trunc = (str, num, type = "...") => (str.length > num ? str.slice(0, num > 3 ? num - 3 : num) + type : str);

const cpfFormat = (val) => {
	return val
		.replace(/\D/g, "")
		.replace(/(\d{3})(\d)/, "$1.$2")
		.replace(/(\d{3})(\d)/, "$1.$2")
		.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
};

const cepFormat = (valor) => {
	var re = /^([\d]{2})\.?([\d]{3})-?([\d]{3})/;
	return re.test(valor) ? valor.trim().replace(re, "$1$2-$3") : valor.trim();
};

const strFormat = (string, ...args) => {
	args.forEach((x) => (string = string.replace("%s", x)));
	return string;
};

const formatFrom = (string, obj) => {
	Object.keys(obj).forEach((x) => {
		string = string.replace(`{${x}}`, `${obj[x]}`);
	});
	return string;
};

const join = (separator = "", ...arr) => arr.reduce((a, e) => (a += `${separator}${e}`));

const fullTrim = (string) => string.replace(/ /gi, "");

const replaceAll = (string, target, replace) => string.replace(new RegExp(target, "g"), replace);

const escapeHTML = (str) =>
	str.replace(
		/[&<>'"]/g,
		(tag) =>
			({
				"&": "&amp;",
				"<": "&lt;",
				">": "&gt;",
				"'": "&#39;",
				'"': "&quot;",
			}[tag] || tag),
	);

export default {
	safe,
	join,
	fullTrim,
	trunc,
	firstName,
	strFormat,
	cepFormat,
	cpfFormat,
	clearHtml,
	formatFrom,
	capitalize,
	currencyFormat,
	floatPrecision,
	escapeHTML,
	replaceAll,
};
